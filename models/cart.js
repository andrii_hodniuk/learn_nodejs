const path = require('path');
const fs = require('fs');
const p = path.join(
	path.dirname(process.mainModule.filename),
	'data',
	'cart.json'
)


class Cart {
	static add = async (course) => {
		const cart = await Cart.fetch();

		const index = cart.courses.findIndex(c => c.id === course.id);
		const isCourse = cart.courses[index];

		if (isCourse) {
			isCourse.count++;
			cart.courses[index] = isCourse;
		} else {
			course.count = 1;
			cart.courses.push(course);
		}

		cart.price = +cart.price + +course.price;

		return new Promise((resolve, reject) => {
			fs.writeFile(
				path.join(__dirname, '..', 'data', 'cart.json'),
				JSON.stringify(cart),
				err => {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				});
		});
	};
	static fetch = async () => {
		return new Promise(((resolve, reject) => {
			fs.readFile(
				path.join(__dirname, '..', 'data', 'cart.json'),
				'utf-8',
				(err, content) => {
					if (err) {
						reject(err);
					} else {
						resolve(JSON.parse(content));
					}
				},
			);
		}));
	};

	static async remove(id) {
		const cart = await Cart.fetch();

		const idx = cart.courses.findIndex(c => c.id === id);
		const course = cart.courses[idx];

		if (course.count === 1) {
			cart.courses = cart.courses.filter(c => c.id !== id);
		} else {
			cart.courses[idx].count--;
		}

		cart.price -= course.price;

		return new Promise((resolve, reject) => {
			fs.writeFile(p, JSON.stringify(cart), err => {
				if (err) {
					reject(err);
				} else {
					resolve(cart);
				}
			});
		});
	}
}

module.exports = Cart;