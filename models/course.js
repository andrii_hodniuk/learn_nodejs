const {v4: uuid} = require('uuid');
const fs = require('fs');
const path = require('path');

class Course {
	constructor(title, price, image) {
		this.title = title;
		this.price = price;
		this.image = image;
		this.id = uuid();
	}

	static getAll = () => {
		return new Promise((resolve, reject) => {
			fs.readFile(
				path.join(__dirname, '..', 'data', 'courses.json'),
				'utf-8',
				(err, content) => {
					if (err) {
						reject(err);
					} else {
						resolve(JSON.parse(content));
					}
				},
			);
		});
	};

	static getById = async id => {
		const courses = await Course.getAll();
		return courses.find(course => course.id === id);
	};

	toObj = () => {
		return {
			title: this.title,
			price: this.price,
			image: this.image,
			id: this.id,
		};
	};

	static update = async (course) => {
		const courses = await Course.getAll();
		const index = courses.findIndex(c => c.id === course.id);
		courses[index] = course;
		return new Promise((resolve, reject) => {
			fs.writeFile(
				path.join(__dirname, '..', 'data', 'courses.json'),
				JSON.stringify(courses),
				err => {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				},
			);
		});

	};

	save = async () => {
		const courses = await Course.getAll();
		courses.push(this.toObj());
		return new Promise((resolve, reject) => {
			fs.writeFile(
				path.join(__dirname, '..', 'data', 'courses.json'),
				JSON.stringify(courses),
				err => {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				},
			);
		});
	};
}

module.exports = Course;